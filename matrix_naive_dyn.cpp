#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

int main()
{

	srand(time(0));
	 
	// using new and delete
	auto start = high_resolution_clock::now();
	
	int m = 1024, n = 1024, c = 0;
	int** a = new int* [m];
	int** b = new int* [m];
	int** result = new int* [m];


	for (int i = 0; i < m; i++)
	{
		a[i] = new int[n];
	}

	for (int i = 0; i < m; i++)
 	{
		for (int j = 0; j < n; j++)
		{
			a[i][j] = 1 + rand() % 5;
		}
	}

	for (int i = 0; i < m; i++)
	{
		b[i] = new int[n];
	}

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			b[i][j] = 1 + rand() % 5;
		}
	}


	cout << a[1][1] << endl;
	for (int i = 0; i < m; i++)
	{
		result[i] = new int[n];
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			for (int k = 0; k < m; k++)
			{
				//cout << " this is a sample" << endl;

				result[i][j] += result[i][k] * result[k][j];
			}
		}
	}

	for (int i = 0; i < m; i++)
		delete[] a[i];
		delete[] a;
	
	for (int i = 0; i < m; i++)
		delete[] b[i];
		delete[] b;
	
	for (int i = 0; i < m; i++)
		delete[] result[i];
		delete[] result;

	auto stop = high_resolution_clock::now();

	auto duration = duration_cast<microseconds>(stop - start);

	cout << "Time taken by function: "
		<< duration.count()/1000000 << " seconds" << endl;


	return 0;
}