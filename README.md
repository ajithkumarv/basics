### Basics

- Matrix naive
    - Using new and delete
    - Vector based approach

- Tiled matrix
    - Vector based approach

- Open MP
    - Vector based approach
    - Vector based tiled approach

- Prime Numbers
    - Naive