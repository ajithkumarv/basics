#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

int main()
{

	srand(time(0));
	
	
	auto start = high_resolution_clock::now();
	// old_implementation
	
	const int n = 100;
	double a[n][n], i, j;
	double b[n][n];
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			a[i][j] = 1 + rand()%5;
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			b[i][j] = 2 + rand()%6;
		}
	}

	cout << "sample number" << a[1][1] << endl;
	
	int mult[n][n];

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			mult[i][j] = 0;
		}
	}

	int k; 

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			for (k = 0; k < n; ++k)
			{
				mult[i][j] += a[i][k] * b[k][j];
			}
		}
	}

	cout << mult[10][20] << endl;

    auto stop = high_resolution_clock::now();

	auto duration = duration_cast<microseconds>(stop - start);

	cout << "Time taken by function: "
		<< duration.count() / 1000000 << " seconds" << endl;
	
	return 0;
}