#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

int main()
{

	srand(time(0));
	 
	// using new and delete
	auto start = high_resolution_clock::now();
	/*
	int m = 1000, n = 1000, c = 0;
	int** a = new int* [m];
	int** b = new int* [m];
	int** result = new int* [m];


	for (int i = 0; i < m; i++)
	{
		a[i] = new int[n];
	}

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			a[i][j] = 1 + rand() % 5;
		}
	}

	for (int i = 0; i < m; i++)
	{
		b[i] = new int[n];
	}

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			b[i][j] = 1 + rand() % 5;
		}
	}


	cout << a[1][1] << endl;
	for (int i = 0; i < m; i++)
	{
		result[i] = new int[n];
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			for (int k = 0; k < m; k++)
			{
				//cout << " this is a sample" << endl;

				result[i][j] += result[i][k] * result[k][j];
			}
		}
	}

	for (int i = 0; i < m; i++)
		delete[] a[i];
		delete[] a;
	
	for (int i = 0; i < m; i++)
		delete[] b[i];
		delete[] b;
	
	for (int i = 0; i < m; i++)
		delete[] result[i];
		delete[] result;

	auto stop = high_resolution_clock::now();

	auto duration = duration_cast<microseconds>(stop - start);

	cout << "Time taken by function: "
		<< duration.count()/1000000 << " seconds" << endl;

	//cout << result[10][10] << endl;

	*/
	// vector implementation
	
	 
	int n = 1000;
	int m = 1000;
	vector<vector<int>> vec_a(n, vector<int>(m, 0));
	vector<vector<int>> vec_b(n, vector<int>(m, 0));
	vector<vector<int>> vec_mul(n, vector<int>(m, 0));
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			vec_a[i][j] = 1 + rand() % 5;
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			vec_b[i][j] = 1 + rand() % 5;
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			for (int k = 0; k < m; k++)
			{
				//cout << " this is a sample" << endl;

				vec_mul[i][j] += vec_a[i][k] * vec_b[k][j];
			}
		}
	}

	cout << "output" << vec_mul[300][400] << endl;
	

	auto stop = high_resolution_clock::now();

	auto duration = duration_cast<microseconds>(stop - start);

	cout << "Time taken by function: "
		<< duration.count() / 1000000 << " seconds" << endl;

	// old_implementation

	//cout << vec[20][30] << endl;

	/*
	const int n = 1000;
	double a[n][n], i, j;
	double b[n][n];
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			a[i][j] = 1 + rand()%5;
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			b[i][j] = 2 + rand()%6;
		}
	}

	cout << "sample number" << a[1][1] << endl;
	
	int mult[n][n];

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			mult[i][j] = 0;
		}
	}

	int k; 

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			for (k = 0; k < n; ++k)
			{
				//cout << " this is a sample" << endl;

				mult[i][j] += a[i][k] * b[k][j];
			}
		}
	}

	cout << mult[10][20] << endl;
	*/
	return 0;
}